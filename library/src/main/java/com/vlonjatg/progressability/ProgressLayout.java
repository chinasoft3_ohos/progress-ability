package com.vlonjatg.progressability;

/**
 * 显示对应布局接口
 *
 * @since 2021-04-07
 */
public interface ProgressLayout {
    /**
     * 显示无参正常界面接口
     */
    void showContent();

    /**
     * 显示有参正常界面接口
     *
     * @param parameterEntity 不隐藏视图id集合
     */
    void showContent(ParameterEntity parameterEntity);

    /**
     * 显示无参加载接口
     */
    void showLoading();

    /**
     * 显示有参加载接口
     *
     * @param parameterEntity 不隐藏视图id集合
     */
    void showLoading(ParameterEntity parameterEntity);

    /**
     * 显示暂无数据界面接口
     *
     * @param parameterEntity 不隐藏视图id集合
     */
    void showEmpty(ParameterEntity parameterEntity);

    /**
     * 显示错误界面接口
     *
     * @param parameterEntity 不隐藏视图id集合
     */
    void showError(ParameterEntity parameterEntity);

    /**
     * 显示正常界面
     *
     * @return String State值
     */
    String getCurrentState();

    /**
     * 显示正常界面
     *
     * @return boolean isState比对结果
     */
    boolean isContentCurrentState();

    /**
     * 显示正常界面
     *
     * @return boolean isState比对结果
     */
    boolean isLoadingCurrentState();

    /**
     * 显示正常界面
     *
     * @return boolean isState比对结果
     */
    boolean isEmptyCurrentState();

    /**
     * 显示正常界面
     *
     * @return boolean isState比对结果
     */
    boolean isErrorCurrentState();
}
