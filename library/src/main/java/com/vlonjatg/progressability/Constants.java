/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vlonjatg.progressability;

/**
 * 常量类
 *
 * @since 2021-04-08
 */
public final class Constants {
    /**
     * LOG_FORMAT
     */
    public static final String LOG_FORMAT = "%{public}s:%{public}s";
    /**
     * 正文类型
     */
    public static final String CONTENT = "type_content";
    /**
     * 加载类型
     */
    public static final String LOADING = "type_loading";
    /**
     * 暂无数据类型
     */
    public static final String EMPTY = "type_empty";
    /**
     * 错误类型
     */
    public static final String ERROR = "type_error";

    private Constants() {
    }
}
