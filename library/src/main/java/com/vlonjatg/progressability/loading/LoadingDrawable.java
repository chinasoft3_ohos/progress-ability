/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vlonjatg.progressability.loading;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;

import ohos.app.Context;

/**
 * 加载Drawable
 *
 * @since 2021-05-12
 */
public class LoadingDrawable extends Image implements Component.DrawTask {
    private LoadingBuilder mLoadingBuilder;

    /**
     * 构造方法
     *
     * @param context 上下文
     */
    public LoadingDrawable(Context context) {
        super(context, null);
    }

    /**
     * 构造方法
     *
     * @param loadingBuilder 加载Builder
     */
    public void setmLoadingBuilder(LoadingBuilder loadingBuilder) {
        this.mLoadingBuilder = loadingBuilder;
    }

    /**
     * 开始
     */
    public void start() {
        this.mLoadingBuilder.start();
    }

    /**
     * 结束
     */
    public void stop() {
        this.mLoadingBuilder.stop();
    }

    @Override
    public int getWidth() {
        return (int) this.mLoadingBuilder.getIntrinsicWidth();
    }

    @Override
    public int getHeight() {
        return (int) this.mLoadingBuilder.getIntrinsicHeight();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        component.setComponentSize(getWidth(), getHeight());
        this.mLoadingBuilder.draw(canvas);
    }
}