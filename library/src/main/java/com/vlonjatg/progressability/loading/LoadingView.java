/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vlonjatg.progressability.loading;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * 加载view
 *
 * @since 2021-05-12
 */
public class LoadingView extends Image implements Component.BindStateChangedListener,
        Element.OnChangeListener, Component.DrawTask {
    protected LoadingBuilder mLoadingBuilder;
    private LoadingDrawable mLoadingDrawable;
    private int mPaintcolor;
    private int mSize;

    /**
     * 构造方法
     *
     * @param context 上下文
     */
    public LoadingView(Context context) {
        this(context, null);
        this.mContext = context;
    }

    /**
     * 构造方法
     *
     * @param context 上下文
     * @param attrs 线下像素
     */
    public LoadingView(Context context, AttrSet attrs) {
        this(context, attrs, "");
        this.mContext = context;
    }

    /**
     * 构造方法
     *
     * @param context 上下文
     * @param attrs 线下像素
     * @param defStyleAttr 定义样式属性
     */
    public LoadingView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 设置画笔颜色
     *
     * @param paintcolor 颜色
     */
    public void setPaintcolor(int paintcolor) {
        this.mPaintcolor = paintcolor;
        initLoadingDrawable();
    }

    /**
     * 设置宽高
     *
     * @param size 宽
     */
    public void setSize(int size) {
        this.mSize = size;
        initLoadingDrawable();
    }

    private void initLoadingDrawable() {
        mLoadingBuilder = new LoadingBuilder();
        mLoadingDrawable = new LoadingDrawable(getContext());
        mLoadingDrawable.setmLoadingBuilder(mLoadingBuilder);
        initParams(getContext(), new Color(mPaintcolor), mSize);
        this.addDrawTask(mLoadingDrawable);
        mLoadingBuilder.setCallback(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        this.mLoadingBuilder.draw(canvas);
    }

    @Override
    public void setVisibility(int visibility) {
        if (getVisibility() != visibility) {
            super.setVisibility(visibility);
            if (visibility == HIDE || visibility == INVISIBLE) {
                startAnimation();
            } else {
                stopAnimation();
            }
        }
    }

    /**
     * 开始动画
     */
    public void startAnimation() {
        if (mLoadingDrawable != null) {
            mLoadingDrawable.start();
        }
    }

    /**
     * 结束动画
     */
    public void stopAnimation() {
        if (mLoadingDrawable != null) {
            mLoadingDrawable.stop();
        }
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        startAnimation();
    }

    void initParams(Context context, Color color, int size) {
        if (mLoadingBuilder != null) {
            mLoadingBuilder.setDefaultSize(size);
            mLoadingBuilder.init(context);
            mLoadingBuilder.initParams(color);

        }
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        stopAnimation();
    }

    @Override
    public void onChange(Element element) {
        this.addDrawTask(mLoadingDrawable);
    }
}

