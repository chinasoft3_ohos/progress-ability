/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vlonjatg.progressability;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;

import java.util.List;

/**
 * 实体类
 *
 * @since 2021-04-08
 */
public class ParameterEntity {
    int iconInt;
    Element iconElement;
    String title;
    String description;
    String buttonText;
    Component.ClickedListener buttonClickListener;
    List<Integer> idsOfViewsNotToHide;

    public int getIconInt() {
        return iconInt;
    }

    public void setIconInt(int iconInt) {
        this.iconInt = iconInt;
    }

    public Element getIconElement() {
        return iconElement;
    }

    public void setIconElement(Element iconElement) {
        this.iconElement = iconElement;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public Component.ClickedListener getButtonClickListener() {
        return buttonClickListener;
    }

    public void setButtonClickListener(Component.ClickedListener buttonClickListener) {
        this.buttonClickListener = buttonClickListener;
    }

    public List<Integer> getIdsOfViewsNotToHide() {
        return idsOfViewsNotToHide;
    }

    public void setIdsOfViewsNotToHide(List<Integer> idsOfViewsNotToHide) {
        this.idsOfViewsNotToHide = idsOfViewsNotToHide;
    }
}
