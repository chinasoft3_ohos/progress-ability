/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vlonjatg.sample.progressability.slice;

import com.vlonjatg.sample.progressability.DetailsAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 文件描述
 *
 * @since 2021-05-27
 */
public class MainAbilitySliceTest {
    private final String key = "STATE";
    private Ability currentTopAbility;
    private Intent intent;
    private Operation operation;

    /**
     * 开始
     */
    @Before
    public void setUp() {
        String testBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        intent = new Intent();
        operation = new Intent.OperationBuilder()
            .withDeviceId("")
            .withBundleName(testBundleName)
            .withAbilityName(DetailsAbility.class.getName())
            .build();
    }

    /**
     * 结束
     */
    @After
    public void tearDown() {
    }

    /**
     * 跳转加载界面测试
     */
    @Test
    public void loadingTest() {
        intent.setOperation(operation);
        intent.setParam(key, "LOADING");
        currentTopAbility.startAbility(intent);
    }

    /**
     * 跳转暂无数据界面测试
     */
    @Test
    public void emptyTest() {
        intent.setOperation(operation);
        intent.setParam(key, "EMPTY");
        currentTopAbility.startAbility(intent);
    }

    /**
     * 跳转错误界面测试
     */
    @Test
    public void errorTest() {
        intent.setOperation(operation);
        intent.setParam(key, "ERROR");
        currentTopAbility.startAbility(intent);
    }

    /**
     * 跳转正常界面测试
     */
    @Test
    public void contentTest() {
        intent.setOperation(operation);
        intent.setParam(key, "CONTENT");
        currentTopAbility.startAbility(intent);
    }
}