/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vlonjatg.sample.progressability.slice;

import com.vlonjatg.progressability.ParameterEntity;
import com.vlonjatg.progressability.ProgressLayout;
import com.vlonjatg.sample.progressability.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.VectorElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * 各种状态下显示的AbilitySlice
 *
 * @since 2021-04-07
 */
public class DetailsAbilitySlice extends AbilitySlice {
    ProgressLayout progressLayout;
    private Text mTextTitle;
    private final Component.ClickedListener errorClickListener = component -> new ToastDialog(getContext())
            .setText("Try again button clicked")
            // Toast显示在界面中间
            .setAlignment(LayoutAlignment.BOTTOM)
            .show();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_details);
        progressLayout = (ProgressLayout) findComponentById(ResourceTable.Id_progress);
        mTextTitle = (Text) findComponentById(ResourceTable.Id_text_title);
        VectorElement emptyDrawable = new VectorElement(this, ResourceTable.Graphic_ic_cart_24dp_white);
        VectorElement errorDrawable = new VectorElement(this, ResourceTable.Graphic_ic_no_connection_24dp_white);
        List<Integer> skipIds = new ArrayList<>();
        skipIds.add(ResourceTable.Id_titleLayout);
        String state = intent.getStringParam("STATE");

        ParameterEntity parameterEntity = new ParameterEntity();
        switch (state) {
            case "LOADING":
                parameterEntity.setIdsOfViewsNotToHide(skipIds);
                progressLayout.showLoading(parameterEntity);
                setTitle("Loading");

                break;
            case "EMPTY":
                parameterEntity.setIconElement(emptyDrawable);
                parameterEntity.setTitle("Empty Shopping Cart");
                parameterEntity.setDescription("Please add things in the cart to continue.");
                parameterEntity.setIdsOfViewsNotToHide(skipIds);
                progressLayout.showEmpty(parameterEntity);
                setTitle("Empty");
                break;

            case "ERROR":
                parameterEntity.setIconElement(errorDrawable);
                parameterEntity.setTitle("No Connection");
                parameterEntity.setDescription(getString(ResourceTable.String_errorContent));
                parameterEntity.setButtonText("Try Again");
                parameterEntity.setButtonClickListener(errorClickListener);
                parameterEntity.setIdsOfViewsNotToHide(skipIds);
                progressLayout.showError(parameterEntity);
                setTitle("Error");
                break;
            case "CONTENT":
                progressLayout.showContent();
                setTitle("Content");
                break;
            default:
        }
        Image mImageFinish = (Image) findComponentById(ResourceTable.Id_image_finish);
        mImageFinish.setClickedListener(component -> terminateAbility());
    }

    /**
     * 设置标题的内容
     *
     * @param title 标题内容
     */
    public void setTitle(String title) {
        mTextTitle.setText(title);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
