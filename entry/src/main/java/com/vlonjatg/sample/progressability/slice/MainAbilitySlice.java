/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vlonjatg.sample.progressability.slice;

import com.vlonjatg.sample.progressability.DetailsAbility;
import com.vlonjatg.sample.progressability.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

/**
 * 主界面AbilitySlice
 *
 * @since 2021-04-07
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_btn_loading).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_empty).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_error).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_content).setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        final String key = "STATE";
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getAbilityPackageContext().getBundleName())
                .withAbilityName(DetailsAbility.class.getName())
                .build();
        switch (component.getId()) {
            case ResourceTable.Id_btn_loading:
                intent.setOperation(operation);
                intent.setParam(key, "LOADING");
                startAbility(intent);
                break;
            case ResourceTable.Id_btn_empty:
                intent.setOperation(operation);
                intent.setParam(key, "EMPTY");
                startAbility(intent);
                break;
            case ResourceTable.Id_btn_error:
                intent.setOperation(operation);
                intent.setParam(key, "ERROR");
                startAbility(intent);
                break;
            case ResourceTable.Id_btn_content:
                intent.setOperation(operation);
                intent.setParam(key, "CONTENT");
                startAbility(intent);
                break;
            default:
        }
    }
}
