﻿﻿# progress-ability
#### 项目介绍
- 项目名称：progress-ability
- 所属系列：openharmony的第三方组件适配移植
- 功能：内容加载时的进度条，空视图，错误视图。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 2.0.5

#### 效果演示
<img src="gif/progress-ability.gif"></img>
#### 安装教程
1.在项目根目录下的build.gradle文件中，

     ```
     allprojects {
        repositories {
            maven {
                url 'https://s01.oss.sonatype.org/content/repositories/releases/'
            }
        }
     }
    ```

2.在entry模块的build.gradle文件中，

     ```
     dependencies {
        implementation('com.gitee.chinasoft_ohos:progress-ability:1.0.0')
        ......  
     }
    ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
根据您的用例，在布局中添加com.vlonjatg.progressability.ProgressDirectionalLayout或com.vlonjatg.progressability.ProgressDependentLayout或com.vlonjatg.progressability.ProgressStackLayout：


       <com.vlonjatg.progressability.ProgressDirectionalLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        xmlns:app="http://schemas.huawei.com/apk/res/ohos"
        ohos:id="$+id:progress"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:orientation="vertical"
        app:emptyBackgroundColor="#fbc02d"
        app:emptyContentTextColor="#FFFFFF"
        app:emptyContentTextSize="14"
        app:emptyImageHeight="308"
        app:emptyImageWidth="308"
        app:emptyTitleTextColor="#FFFFFF"
        app:emptyTitleTextSize="15"
        app:errorBackgroundColor="#42A5F5"
        app:errorButtonBackgroundColor="#9C27B0"
        app:errorButtonTextColor="#FFFFFF"
        app:errorContentTextColor="#FFFFFF"
        app:errorContentTextSize="14"
        app:errorImageHeight="308"
        app:errorImageWidth="308"
        app:errorTitleTextColor="#FFFFFF"
        app:errorTitleTextSize="15"
        app:loadingBackgroundColor="#FFFFFF"
        app:loadingProgressBarColor="#ff9C27B0">
        
        <Text
            ohos:id="$+id:text_hello"
            ohos:height="0vp"
            ohos:width="match_content"
            ohos:layout_alignment="center"
            ohos:text="hello_world"
            ohos:text_size="15fp"
            ohos:weight="1"/>
            
    </com.vlonjatg.progressability.ProgressDirectionalLayout>

然后在您的代码中：

    ParameterEntity parameterEntity = new ParameterEntity();

要显示加载视图：
skipIds：标题控件ID

    parameterEntity.setIdsOfViewsNotToHide(skipIds);
    progressLayout.showLoading(parameterEntity);

要显示空视图：

    parameterEntity.setIconElement(emptyDrawable);
    parameterEntity.setTitle("Empty Shopping Cart");
    parameterEntity.setDescription("Please add things in the cart to continue.");
    parameterEntity.setIdsOfViewsNotToHide(skipIds);
    progressLayout.showEmpty(parameterEntity);

要显示错误视图：

    parameterEntity.setIconElement(errorDrawable);
    parameterEntity.setTitle("No Connection");
    parameterEntity.setDescription(getString(ResourceTable.String_errorContent));
    parameterEntity.setButtonText("Try Again");
    parameterEntity.setButtonClickListener(errorClickListener);
    parameterEntity.setIdsOfViewsNotToHide(skipIds);
    progressLayout.showError(parameterEntity);

要显示内容，请使用：

    progressLayout.showContent();

自定义ConvenientBanner控件，可以添加自定义属性，具体可用属性如下:

| name                    | type      | info                                                   |
|-------------------------|-----------|--------------------------------------------------------|
| loadingStateProgressBarWidth    | int       | 设置加载宽度                                   |
| loadingStateProgressBarHeight   | int       | 设置加载高度                                   |
| loadingProgressBarColor         | int       | 设置加载颜色                                   |
| loadingBackgroundColor          | int       | 设置加载界面背景颜色                           |
| emptyImageWidth                 | int       | 设置空界面图标宽度                             |
| emptyImageHeight                | int       | 设置空界面图标高度                             |
| emptyTitleTextSize              | int       | 设置空界面title字体大小                        |
| emptyTitleTextColor             | Color     | 设置空界面title字体颜色                        |
| emptyContentTextSize            | int       | 设置空界面Content字体大小                      |
| emptyContentTextColor           | Color     | 设置空界面Content字体颜色                      |
| emptyBackgroundColor            | int       | 设置空界面背景颜色                             |
| errorImageWidth                 | int       | 设置错误界面图标宽度                           |
| errorImageHeight                | int       | 设置错误界面图标高度                           |
| errorTitleTextSize              | int       | 设置错误界面title字体大小                      |
| errorTitleTextColor             | Color     | 设置错误界面title字体颜色                      |
| errorContentTextSize            | int       | 设置错误界面Content字体大小                    |
| errorContentTextColor           | Color     | 设置错误界面Content字体颜色                    |
| errorButtonTextColor            | Color     | 设置错误界面Button字体颜色                     |
| errorButtonBackgroundColor      | int       | 设置错误界面Button背景颜色                     |
| errorBackgroundColor            | int       | 设置错误界面背景颜色                           |

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0
#### 版权和许可信息

    The MIT License (MIT)
    
    Copyright (c) 2015 Vlonjat Gashi
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

